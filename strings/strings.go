package main

import (
	"fmt"
	"strings"
)

func main() {

	var s string
	s = "Hello World"
	fmt.Println(strings.ToUpper(s))
	fmt.Println(s[0])
	fmt.Println(string(s[0]))

	r := strings.Replace(s, "-", " ", -1)
	words := strings.Split(r, " ")
	var acr string
	for _, word := range words {
		acr = acr + string(word[0])
	}
	fmt.Println(strings.ToUpper(acr))

	for i := 0; i < len(s); i++ {
		fmt.Printf("%d > %s\n", s[i], string(s[i]))
	}

	fmt.Printf("double quotet string %%q: %q\n", s)
	fmt.Printf("single quotet character %%q: %q\n", s[0])
}
