package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type sourceDir struct {
	Dir      string   `json:"dir"`
	Excludes []string `json:"exclude"`
}

type config struct {
	Dirs      []sourceDir `json:"Dirs"`
	RsyncCmd  string      `json:"rsync_cmd"`
	RsyncArgs string      `json:"rsync_args"`
	TargetDir string      `json:"target_dir"`
}

func (c config) printDirs() {
	for i, d := range c.Dirs {
		dir := c.Dirs[i].Dir
		fmt.Printf("%s\n", dir)
		for _, excl := range d.Excludes {
			fmt.Printf("  -%s\n", excl)
		}
	}
}

func toJson(c interface{}) string {
	bytes, err := json.Marshal(c)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return string(bytes)
}

func getConfig() config {
	raw, err := ioutil.ReadFile("./config.json")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	var c config
	json.Unmarshal(raw, &c)
	return c
}

func main() {
	conf := config{
		Dirs: []sourceDir{
			{Dir: "/etc", Excludes: []string{"apache22", "vimrc", "ssh"}},
			{Dir: "/home/pkruesi/tmp", Excludes: []string{"foobar", "blum frup"}},
			{Dir: "/home/pkruesi/Downloads", Excludes: []string{}},
			{Dir: "/home/pkruesi/Videos", Excludes: []string{}},
		},
		RsyncCmd:  "/usr/bin/rsync",
		RsyncArgs: "-a",
		TargetDir: "/backup/",
	}

	for _, dir := range conf.Dirs {
		fmt.Println(toJson(&dir))
	}
	fmt.Println("")
	fmt.Println("Outpout config struct as json from var \"conf\"")
	fmt.Println(toJson(&conf))

	fmt.Println("")
	fmt.Println("Outpout config struct as json read from ./config.json")
	jsonconf := getConfig()
	for _, dir := range jsonconf.Dirs {
		fmt.Println(toJson(&dir))
	}
	fmt.Println(toJson(&jsonconf))

	fmt.Println("Print Dirs")
	jsonconf.printDirs()
}
