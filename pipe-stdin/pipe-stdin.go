package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	fmt.Println("stdin")

	info, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	if info.Mode()&os.ModeCharDevice == os.ModeCharDevice {
		fmt.Println("no pipe!")
		os.Exit(1)
	}

	reader := bufio.NewReader(os.Stdin)

	for {
		line, _, err := reader.ReadLine()
		if err != nil && err == io.EOF {
			break
		}
		fmt.Printf("> %s\n", line)

		fmt.Fprintln(os.Stderr, "ERROR:"+string(line))
	}

}
