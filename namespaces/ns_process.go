/*
Namespace tutorial:
- https://medium.com/@teddyking/linux-namespaces-850489d3ccf
*/

package main

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"

	"github.com/docker/docker/pkg/reexec"
)

func init() {
	fmt.Printf("init()\n")
	fmt.Printf("os.Args[1]: %s\n", os.Args[1])
	reexec.Register("nsInitialisation", nsInitialisation)
	if reexec.Init() {
		fmt.Printf("reexec init() --> os.Exit(0)\n")
		os.Exit(0)
	}
}

func nsInitialisation() {
	fmt.Printf("nsInitialisation()\n")
	//fmt.Printf("os.Args[1]: %s\n", os.Args[1])
	newrootPath := os.Args[1]
	fmt.Printf("newrootPath before if: %s\n", newrootPath)

	if newrootPath == "" {
		newrootPath = "/tmp/ns-process/rootfs/"
	}
	fmt.Printf("newrootPath after if: %s\n", newrootPath)

	if err := mountProc(newrootPath); err != nil {
		fmt.Printf("Error mounting /proc - %s\n", err)
		os.Exit(1)
	}

	if err := pivotRoot(newrootPath); err != nil {
		fmt.Printf("Error running pivot_root - %s\n", err)
		os.Exit(1)
	}

	nsRun()
}

func nsRun() {
	fmt.Printf("nsRun()\n")
	cmd := exec.Command("/bin/sh")

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	cmd.Env = []string{"PS1=-[ns-process]- # "}

	if err := cmd.Run(); err != nil {
		fmt.Printf("Error running the /bin/sh command - %s\n", err)
		os.Exit(1)
	}
}

func main() {
	fmt.Printf("main()\n")
	var rootfsPath string
	cmd := reexec.Command("nsInitialisation", rootfsPath)
	//cmd := exec.Command("/bin/sh")

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	cmd.Env = []string{"PS1=-[ns-process]- # "}

	cmd.SysProcAttr = &syscall.SysProcAttr{
		// create new namespaces
		Cloneflags: syscall.CLONE_NEWUTS |
			syscall.CLONE_NEWNS |
			syscall.CLONE_NEWIPC |
			syscall.CLONE_NEWPID |
			syscall.CLONE_NEWNET |
			syscall.CLONE_NEWUSER,
		// for uid mapping in the real world
		UidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      os.Getuid(),
				Size:        1,
			},
		},
		// for uid mapping in the real world
		GidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      os.Getgid(),
				Size:        1,
			},
		},
	}

	if err := cmd.Run(); err != nil {
		fmt.Printf("Error running the /bin/sh command - %s\n", err)
		os.Exit(1)
	}
}
