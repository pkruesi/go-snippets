package temperature

import "fmt"

type temp struct {
  temp float64
  celsius float64
  fahrenheit float64
}

func NewTemp(temperature float64) temp{
  te := temp{temp: temperature, celsius: 0.0, fahrenheit: 0.0 }
  te.convert()
  return te
}

func (t *temp) convert() {
  t.celsius = (t.temp -32) * 5/9
  t.fahrenheit = (t.temp *9/5) +32
}

func (t *temp) Convert(temperature float64) {
  t.temp = temperature
  t.convert()
}

func (t temp) String() string {
  return fmt.Sprintf("%f Fahrenheit as Celsius: %f\n%f Celsius as Fahrenheit: %f", t.temp, t.celsius, t.temp, t.fahrenheit)
}
