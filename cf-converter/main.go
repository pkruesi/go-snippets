package main

import (
	"cf-converter/temperature"
	"flag"
	"fmt"
)

var temp float64

func init() {
  flag.Float64Var(&temp, "t", 0.0, "temperature as float")
  flag.Parse()
}

func main() {
  myTemp := temperature.NewTemp(temp)
  fmt.Println(myTemp)
  fmt.Println()
  fmt.Println("Using 'Convert(temperature)' function from module 'temperature'")
  fmt.Println("Convert 33 to Fahrenheit and Celcius")
  myTemp.Convert(33)
  fmt.Println(myTemp)
}
