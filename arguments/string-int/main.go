package main

import (
	"flag"
	"fmt"
)

// global pointer vars
var (
	env *string
	port *int
)

func init(){
	env = flag.String("env", "development", "current environment")
	port = flag.Int("port", 3000, "port number")
}

func printer(){
	fmt.Printf("env[%s] port[%d]\n", *env, *port)
}

func main() {

	flag.Parse()
	fmt.Println("env:", *env)
	fmt.Println("port:", *port)
	printer()

}
