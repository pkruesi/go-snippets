package main

import (
	"flag"
	"fmt"
	"strings"
)

func main() {
	var name string
	var upper bool

	flag.StringVar(&name, "name", "guest", "your name")
	flag.BoolVar(&upper, "u", false, "display in uppercase")
	flag.Parse()

	var msg string
	msg = fmt.Sprintf("Hello %s", name)
	if upper {
		fmt.Println(strings.ToUpper(msg))
	} else {
		fmt.Println(msg)
	}
}
