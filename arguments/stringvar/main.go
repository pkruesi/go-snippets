package main

import (
	"flag"
	"fmt"
)

func main() {
	var name string
	flag.StringVar(&name, "name", "guest", "your name")
	flag.Parse()

	fmt.Printf("Hello %s\n", name)
}
