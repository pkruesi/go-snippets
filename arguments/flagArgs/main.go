package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {
	var u bool
	var q bool

	flag.BoolVar(&u, "u", false, "display in uppercase")
	flag.BoolVar(&q, "q", false, "quote the string")
	flag.Parse()

	values := flag.Args()

	if len(values) == 0 {
		fmt.Println("Usage: ...")
		flag.PrintDefaults()
		os.Exit(1)
	}

	for _, word := range values {
		if u {
			word = strings.ToUpper(word)
		} 

		// works only if "-q" is bevor "-u" !!
		if q {
			word = fmt.Sprintf("\"%s\"", word)
		}

		fmt.Println(word)
	}
}
