package main

import (
	"flag"
	"fmt"
)

func main() {
	num:= flag.Int("n", 5, "# of iterations")
	flag.Parse()

	n:= *num
	i:=0

	for i<n {
		fmt.Printf("n: %d\n", i)
		i++
	}
}
