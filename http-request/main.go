package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"
)

var (
	url string
)

func init() {
	flag.StringVar(&url, "u", "https://1102.ch", "url to connect")
	flag.Parse()
}

// struct to store start and stop time
type usedTime struct {
	startTime time.Time
	stopTime  time.Time
}

// set the start time for the struct usedTime{...}
func (ut *usedTime) start() {
	ut.startTime = time.Now()
}

// sets the stop time for the struct usedTime{...}
// prints the elapsed time between ut.startTime and ut.stopTime in milliseconds
func (ut *usedTime) stop() {
	ut.stopTime = time.Now()
	fmt.Printf("> milliseconds used: %d\n", ut.stopTime.UnixMilli()-ut.startTime.UnixMilli())
}

func main() {

	stopwatch := usedTime{}

	fmt.Println("--- http.get")
	stopwatch.start()
	resp, err := http.Get(url)
	if err != nil {
		log.Print(err)
	} else {
		defer resp.Body.Close()

		fmt.Println(resp.StatusCode, resp.Status)
		fmt.Println("Content-Lenght:", resp.ContentLength)
		fmt.Println(resp.Header)
	}
	stopwatch.stop()
	fmt.Println("--- client.Get - not follow redirects set, http.client timeout 15s")
	stopwatch.start()

	// default client.Timeout = 30
	client := &http.Client{
		Timeout: 15 * time.Second,
	}
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}
	resp, err = client.Get(url)
	if err != nil {
		log.Print(err)
	} else {
		defer resp.Body.Close()
		fmt.Println(resp.StatusCode, resp.Status)
		fmt.Println("Content-Lenght:", resp.ContentLength)
		fmt.Println(resp.Header)
	}
	stopwatch.stop()

	// some hints https://gist.github.com/mobleyc/2e1d62a08d6a4e64d18d
	fmt.Println("--- client with transport - tcp connect timeout 2s")
	stopwatch.start()
	tr := &http.Transport{
		DisableKeepAlives: true,
		Dial: (&net.Dialer{
			Timeout:   2 * time.Second,
			KeepAlive: 10 * time.Second,
		}).Dial,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		TLSHandshakeTimeout: 10 * time.Second,
	}

	tr.CloseIdleConnections()
	req, _ := http.NewRequest("GET", url, nil)
	req.Close = false
	resp, err = tr.RoundTrip(req)
	if err != nil {
		log.Print(err)
	} else {
		defer resp.Body.Close()
		fmt.Println(resp.StatusCode, resp.Status)
		fmt.Println("Content-Lenght:", resp.ContentLength)
		fmt.Println(resp.Header)
	}
	stopwatch.stop()

}
