package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"
)

type rFile struct {
	Name     string        `json:"name"`
	Bytes    int           `json:"bytes"`
	Duration time.Duration `json:"duration"`
}

// GetFiles returns a list of found files
//recursivly found in given directory
func GetFiles(dir string) ([]string, error) {
	var files []string
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})
	return files, err
}

func FillFilesChan(files []string, fillChan chan string) {
	for _, f := range files {
		fillChan <- f
		fmt.Printf("fillChan<- %s\n", f)
	}
	close(fillChan)
}

func ReadFileWorkerPool(readChan chan string, count int) {
	for i := 0; i < count; i++ {
		s := <-readChan
		fmt.Printf("File: %s\n", s)
		fmt.Printf("res: %v\n", ReadFile(s))
	}
}

// readFile reads the given file and returns the filename,
//amout of red bytes and duration
func ReadFile(fName string) rFile {
	fmt.Printf("inFile %s\n", fName)
	start := time.Now()

	f, err := os.Open(fName)
	if err != nil {
		log.Printf("%s\n", err)
	}

	defer func() {
		err = f.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	r := bufio.NewReader(f)
	fileChunk := make([]byte, 1024)

	chunk := 0
	size := 0
	for {
		s, err := r.Read(fileChunk)
		if err != nil {
			if err == io.EOF {
				log.Println(err)
				break
			} else {
				log.Fatalf("Error read file %s, %v\n", fName, err)

				break
			}
		}
		size += s

		chunk++
	}
	elapsed := time.Since(start)

	rf := rFile{
		Name:     fName,
		Bytes:    size,
		Duration: elapsed,
	}

	return rf
}
