package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

var rPath string

func init() {

	const (
		default_rPath = "file"
		usage_rPath   = "path to read\n file: file gets read\n dir: all files in dir gets read"
	)

	flag.StringVar(&rPath, "path", default_rPath, usage_rPath)
	flag.StringVar(&rPath, "p", default_rPath, usage_rPath+" (shorthand)")

	flag.Parse()
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed)
}

func main() {
	count := 3
	fChan := make(chan string, count)
	var files []string

	pInfo, _ := os.Stat(rPath)
	if pInfo.IsDir() == true {
		files, _ = GetFiles(rPath)
	} else {
		files = append(files, rPath)
	}

	go FillFilesChan(files, fChan)
	go ReadFileWorkerPool(fChan, count)

	fmt.Scanln()

	//files, _ := GetFiles(rDir)
	//fmt.Printf("%v\n", files)

	rf := ReadFile(rPath)
	rfJson, _ := json.Marshal(rf)
	fmt.Printf("%s\n", rfJson)
}
