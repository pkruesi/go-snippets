package debugger

import "fmt"

// PrintDebug prints "DEBUG" in front of string
func PrintDebug(str string) {
	fmt.Printf("DEBUG: %s\n", str)
}
