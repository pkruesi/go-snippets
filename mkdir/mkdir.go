package main

import (
    "fmt"
    "os"
)


func main() {
    fmt.Println("mkdir /tmp/foo")
    os.Mkdir("/tmp/foo", 0700)
    os.Mkdir("/tmp/foo/", 0700) 
}
