package main

import (
	"fmt"
)

func toChannel(ch chan byte, str string) {
	barray := []byte(str)
	for _, b := range barray {
		ch <- b
	}
	close(ch)
}

func fromChannel1(ch chan byte, done chan bool) {
	for b := range ch {
		fmt.Printf("go 1, '%c'\n", b)
	}
	done <- true
}

func fromChannel2(ch chan byte, done chan bool) {
	for b := range ch {
		fmt.Printf("go 2, '%c\n", b)
	}
	done <- true
}

func main() {
	fmt.Println("Channels")

	done := make(chan bool, 3)

	ch1 := make(chan byte, 2)

	hw := "hello world"
	go toChannel(ch1, hw)
	go fromChannel1(ch1, done)
	go fromChannel2(ch1, done)

	<-done
	<-done
	//<-done
	fmt.Println("done")
}
