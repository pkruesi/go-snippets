package linker

import (
	"fmt"
	"os"
)

func LinkFile(filename, destDir string) error {

	fileInfo, err := os.Stat(filename)

	dest := destDir + "/" + filename

	err = os.MkdirAll(destDir, fileInfo.Mode().Perm())

	fmt.Println(err)

	err = os.Link(filename, dest)

	fmt.Println(err)

	return err
}
