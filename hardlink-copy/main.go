package main

import (
	"fmt"
	"hardlink-copy/linker"
)

func main() {
	fmt.Println(">> start")

	sourcefile := "go.mod"
	destdir := "/home/pkruesi/tmp/go.mod-hardlinked"

	err := linker.LinkFile(sourcefile, destdir)
	fmt.Printf("> %s\n", err)
}
