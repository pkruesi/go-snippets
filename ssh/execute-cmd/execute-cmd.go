package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"syscall"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"golang.org/x/crypto/ssh/terminal"
)

/*
Todo:
	* check ssh.KeyboardInteractive
		* see https://stackoverflow.com/questions/47102080/ssh-in-go-unable-to-authenticate-attempted-methods-none-no-supported-method?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
*/

func main() {

	authAgent := flag.Bool("A", false, "using ssh agent for auth")
	pwd := flag.String("password", "", "sets the password for ssh connection")
	//user := flag.String("user", os.Getenv("USER"), "sets the username for ssh connection")
	user := flag.String("user", "", "sets the username for ssh connection")
	host := flag.String("host", "", "sets the host to which we will connect")
	flag.Parse()

	var sshConfig ssh.ClientConfig
	sshConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	var username string
	var password string

	if *user == "" {
		fmt.Printf("username: ")
		fmt.Scanf("%s", &username)
	} else {
		username = *user
	}
	sshConfig.User = username

	if *authAgent == false {

		if *pwd == "" {
			fmt.Printf("password: ")
			p, err := terminal.ReadPassword(int(syscall.Stdin))
			password = string(p)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("\n")
		} else {
			password = *pwd
		}
		sshConfig.Auth = []ssh.AuthMethod{ssh.Password(password)}

	} else {
		sock, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK"))
		if err != nil {
			log.Fatal(err)
		}

		agent := agent.NewClient(sock)

		signers, err := agent.Signers()
		if err != nil {
			log.Fatal(err)
		}

		sshConfig.Auth = []ssh.AuthMethod{ssh.PublicKeys(signers...)}
	}

	//sshConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()
	//sshConfig.User = "pkruesi"
	//sshConfig.Auth = []ssh.AuthMethod{ssh.Password("born1976.pkruesi")}

	// create a connection to a host with the given sshConfig
	//connection, err := ssh.Dial("tcp", "worker.1102.ch:22", &sshConfig)
	// Todo: connection to localhost is not working!
	connection, err := ssh.Dial("tcp", *host+":22", &sshConfig)
	if err != nil {
		log.Fatalf("unable to create connection: %v", err)
	}

	session, err := connection.NewSession()
	if err != nil {
		log.Fatalf("unable to create session: %v", err)
	}
	defer session.Close()

	var b bytes.Buffer
	session.Stdout = &b
	if err := session.Run("/usr/bin/w"); err != nil {
		log.Fatal("Failed to run: " + err.Error())
	}
	fmt.Println(b.String())

}
