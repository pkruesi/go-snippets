package main

import (
	"database/sql"
	"go-echo-vue/todo/handlers"

	"github.com/labstack/echo"
	_ "github.com/mattn/go-sqlite3"
)

func initDB(filepath string) *sql.DB {
	db, err := sql.Open("sqlite3", filepath)

	// Here we check fo any error then exit
	if err != nil {
		panic(err)
	}

	// if we don't get any errors but somehow still don't get a db connection
	// we exit as well
	if db == nil {
		panic("db nil")
	}

	return db
}

func migrate(db *sql.DB) {
	sql := `
	CREATE TABLE IF NOT EXISTS tasks(
		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		name VARCHAR NOT NULL
	);
	`

	_, err := db.Exec(sql)
	// Exit if something goes wrong with our SQL statement above
	if err != nil {
		panic(err)
	}
}

func main() {

	db := initDB("storage.db")
	migrate(db)

	e := echo.New()
	e.File("/", "public/index.html")

	/*
		Looking at this code, you may notice that the handlers listed
		don't actually follow the function signature required by Echo.
		Instead these are functions that return a function
		that satisfy that interface.
		This is a trick I used so we can pass around the db instance
		from handler to handler without having to create a new one each time
		we want to use the database. It'll be come more clear later.
	*/
	e.GET("/tasks", handlers.GetTasks(db))
	e.PUT("/tasks", handlers.PutTask(db))
	e.DELETE("/tasks/:id", handlers.DeleteTask(db))

	e.Logger.Fatal(e.Start(":8000"))
}
