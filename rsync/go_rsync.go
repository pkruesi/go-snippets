package main

import (
	"fmt"
	"os/exec"
	"strings"
	"time"
)

type sourceDir struct {
	Dir      string
	Excludes []string
}

type config struct {
	Dirs      []sourceDir
	RsyncCmd  string
	RsyncArgs string
	TargetDir string
}

func getConfig() config {

	// this part should be read from a config file, config.json or something else
	conf := config{
		Dirs: []sourceDir{
			sourceDir{Dir: "/etc", Excludes: []string{"apache22", "vimrc", "ssh"}},
			sourceDir{Dir: "/home/pkruesi/tmp", Excludes: []string{"bashbunny-payloads", "blum frup"}},
			sourceDir{Dir: "/home/pkruesi/Downloads", Excludes: []string{}},
			sourceDir{Dir: "/home/pkruesi/porteus", Excludes: []string{}},
		},
		RsyncCmd:  "/usr/bin/rsync",
		RsyncArgs: "-a",
		TargetDir: "/backup/",
	}
	return conf
}

func rsync(command string, args string, sd sourceDir, targetDir string) {

	var excludeOpts string
	//excludeOpts = ""

	if sd.Excludes != nil {
		for _, ex := range sd.Excludes {
			excludeOpts += " --exclude='" + ex + "'"
		}
		excludeOpts = strings.TrimSpace(excludeOpts)
	}
	fmt.Printf("> Backupjob: %s %s\n", sd.Dir, time.Now())

	bashArg := "-c"
	rsyncArgs := command + " " + args + " " + excludeOpts + " " + sd.Dir + " " + targetDir

	start := time.Now()

	cmd := exec.Command("bash", bashArg, rsyncArgs)
	stdouterr, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Printf(">> ERROR: %v\n", err)
		fmt.Printf(">> Backupcommand: %s %s %s %s %s\n", command, args, excludeOpts, sd.Dir, targetDir)
		fmt.Printf(">> Output:\n%s\n", stdouterr)
	} else {
		fmt.Printf("backup ok for %s\n", sd.Dir)
	}
	fmt.Printf("Duration: %s\n", time.Now().Sub(start))
	fmt.Printf("\n")
}

func main() {
	fmt.Println("> start backupjobs")

	conf := getConfig()

	for i := 0; i < len(conf.Dirs); i++ {
		rsync(conf.RsyncCmd, conf.RsyncArgs, conf.Dirs[i], conf.TargetDir)
	}

	fmt.Println("> end backupjobs")

}
