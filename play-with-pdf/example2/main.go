package main

import (
	"fmt"
	"strconv"
	"time"

	"github.com/jung-kurt/gofpdf"
)

type Adresse struct {
	Firma       string
	VornameName string
	Strasse     string
	Hausnummer  string
	PLZ         string
	Ort         string
}

type Text struct {
	Betreff      string
	Anrede       string
	Kuendigung   string
	Bedankung    string
	Zeugnis      string
	Gruss        string
	Erhalten     string
	DatumOrt     string
	Unterschrift string
}

func kuendigungPer(frist int) time.Time {
	now := time.Now()
	currentYear, currentMonth, _ := now.Date()
	currentLocation := now.Location()

	firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	lastOfMonth := firstOfMonth.AddDate(0, frist+1, -1)

	return lastOfMonth
}

func main() {

	kFrist := 3
	kPerDate := kuendigungPer(kFrist)
	kPer := kPerDate.Format("02.01.2006")

	absender := Adresse{
		Firma:       "",
		VornameName: "Pirmin Kruesi",
		Strasse:     "Seestrasse",
		Hausnummer:  "9",
		PLZ:         "6314",
		Ort:         "Unteraegeri",
	}

	empfaenger := Adresse{
		Firma:       "SuperInc.",
		VornameName: "OberCheffe",
		Strasse:     "Bahnhofstrasse",
		Hausnummer:  "9",
		PLZ:         "8001",
		Ort:         "Zuerich",
	}

	text := Text{
		Betreff:      "Ich mache ein sogenanner 'Abhaui' per " + kPer,
		Anrede:       "Sehr geehrte Damen und Herren",
		Kuendigung:   "hiermit kuendige ich meine Anstellung unter Einhaltung der vertraglich abgemachten Kuendigungsfrist von " + strconv.Itoa(kFrist) + " Monate per " + kPer + ".",
		Bedankung:    "Ich bedanke mich herzlich fuer die gute Zusammenarbeit und die schoene Zeit die ich bei " + empfaenger.Firma + " erleben durfte.",
		Zeugnis:      "Bitte stellen Sie mir ein Arbeitszeugnis bis Ende " + kPerDate.Month().String() + " " + kPerDate.Format("2006") + " zu.",
		Gruss:        "Freundliche Gruesse",
		Erhalten:     "Kuendigung erhalten",
		DatumOrt:     "Datum / Ort:",
		Unterschrift: "Unterschrift Empfaenger:",
	}

	pdf := gofpdf.New("P", "mm", "A4", "") // A4 210.0 x 297.0
	pdf.SetFont("Arial", "", 14)
	pdf.AddPage()
	pdf.SetMargins(10, 10, 30)

	_, lineHt := pdf.GetFontSize()
	tabstop1 := 130.0
	tabstop2 := 30.0
	pageWd, _ := pdf.GetPageSize() //210.0, 297.0
	lineWd := pageWd - 20.0

	pdf.SetXY(10, 4*lineHt)
	pdf.CellFormat(100.0, lineHt, absender.VornameName, "", 1, "L", false, 0, "")
	pdf.CellFormat(100.0, lineHt, absender.Strasse+" "+absender.Hausnummer, "", 1, "L", false, 0, "")
	pdf.CellFormat(100.0, lineHt, absender.PLZ+" "+absender.Ort, "", 0, "L", false, 0, "")

	date := time.Now()
	dateString := date.Format("02.01.2006")

	pdf.SetXY(tabstop1, pdf.GetY())
	pdf.CellFormat(lineWd-tabstop1, lineHt, absender.Ort+", den "+dateString, "", 1, "L", false, 0, "")
	pdf.SetXY(tabstop1, pdf.GetY()+lineHt)

	pdf.CellFormat(lineWd-tabstop1, lineHt, empfaenger.Firma, "", 2, "L", false, 0, "")
	pdf.CellFormat(lineWd-tabstop1, lineHt, empfaenger.VornameName, "", 2, "L", false, 0, "")
	pdf.CellFormat(lineWd-tabstop1, lineHt, empfaenger.Strasse+" "+empfaenger.Hausnummer, "", 2, "L", false, 0, "")
	pdf.CellFormat(lineWd-tabstop1, lineHt, empfaenger.PLZ+" "+empfaenger.Ort, "", 2, "L", false, 0, "")

	pdf.SetY(pdf.GetY() + 2*lineHt)
	pdf.SetFont("Arial", "B", 14)
	pdf.CellFormat(lineWd, lineHt, text.Betreff, "", 1, "L", false, 0, "")
	pdf.SetFont("Arial", "", 14)
	pdf.SetY(pdf.GetY() + lineHt)
	pdf.CellFormat(lineWd, lineHt, text.Anrede, "", 1, "L", false, 0, "")
	pdf.SetY(pdf.GetY() + lineHt)
	pdf.MultiCell(lineWd, lineHt, text.Kuendigung, "", "L", false)
	pdf.SetY(pdf.GetY() + lineHt)
	pdf.MultiCell(lineWd, lineHt, text.Bedankung, "", "L", false)
	pdf.SetY(pdf.GetY() + lineHt)
	pdf.MultiCell(lineWd, lineHt, text.Zeugnis, "", "L", false)
	pdf.SetY(pdf.GetY() + lineHt)
	pdf.SetXY(tabstop2, pdf.GetY()+lineHt)
	pdf.CellFormat(50.0, lineHt, text.Gruss, "", 1, "R", false, 0, "")
	pdf.SetY(pdf.GetY() + lineHt)
	pdf.SetXY(tabstop2, pdf.GetY()+lineHt)
	pdf.CellFormat(50.0, lineHt, absender.VornameName, "", 1, "R", false, 0, "")
	pdf.SetY(pdf.GetY() + 10*lineHt)
	pdf.SetLineWidth(0.1)
	pdf.SetLineCapStyle("round")
	pdf.Line(10.0, pdf.GetY(), pageWd-10.0, pdf.GetY())
	pdf.SetXY(tabstop2, pdf.GetY()+lineHt)
	pdf.CellFormat(50.0, lineHt, text.Erhalten, "", 1, "R", false, 0, "")
	pdf.SetXY(tabstop2, pdf.GetY()+lineHt)
	pdf.CellFormat(50.0, lineHt, text.DatumOrt, "", 1, "R", false, 0, "")
	pdf.SetXY(tabstop2, pdf.GetY()+lineHt)
	pdf.CellFormat(50.0, lineHt, text.Unterschrift, "", 1, "R", false, 0, "")

	err := pdf.OutputFileAndClose("example2.pdf")
	fmt.Errorf("ERROR: %s\n", err)
}
