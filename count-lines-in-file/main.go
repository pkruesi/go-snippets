package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
)

func LineCounter(r io.Reader) (int, error) {

	var count int
	const lineBreak = '\n'

	buf := make([]byte, bufio.MaxScanTokenSize)

	for {
		bufferSize, err := r.Read(buf)
		if err != nil && err != io.EOF {
			return 0, err
		}

		var buffPosition int
		for {
			i := bytes.IndexByte(buf[buffPosition:], lineBreak)
			if i == -1 || bufferSize == buffPosition {
				break
			}
			buffPosition += i + 1
			count++
		}
		if err == io.EOF {
			break
		}
	}

	return count, nil
}

func main() {
	fmt.Println("vim-go")
	file, err := os.Open("./testfile")
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	r := bufio.NewReader(file)
	lines, err := LineCounter(r)
	if err != nil {
		log.Panic(err)
	}
	fmt.Printf("file %s: %d lines\n", file.Name(), lines)

}
