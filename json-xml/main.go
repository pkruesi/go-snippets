package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
)

type Person struct {
	Lastname  string `json:"lastname" xml:"id,attr"`
	Firstname string `json:"firstname" xml:"parent>child"`
}

func main() {
	pers := Person{Lastname: "Doe", Firstname: "Jhon"}
	fmt.Printf("%v\n", pers)

	byteJson, _ := json.Marshal(pers)
	fmt.Printf("%s\n", string(byteJson))

	var pers2 Person
	json.Unmarshal(byteJson, &pers2)
	fmt.Printf("%v\n", pers2)

	byteXml, _ := xml.MarshalIndent(pers2, "", "  ")
	fmt.Printf("%s\n", string(byteXml))

	var pers3 Person
	xml.Unmarshal(byteXml, &pers3)
	fmt.Printf("%v\n", pers3)

}
