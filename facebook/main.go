package main

import (
	"fmt"

	fb "github.com/huandu/facebook"
)

func main() {

	/*
		* get an accessToken from:
			https://developers.facebook.com/tools/explorer
			* choose there your app, an grant the permissions
	*/
	accessToken := "EAAHLD1GPrLEBALxGRcZCHBlM8Uzsv3ZCWrghYaXHkwkehpZAGqJSmlzxuyAXFZCvxwyocZAZCTfftm7ZCguojJCgbSV0zDMEuiuZC0YkKIbZCZA6QF3gH4fr0jUv6XCwEfv1qsNJLxbF95MOZCujsWAejxR73PlkzuaFqgwyHBo9vvD8ZCDKZBH6HUWWHlnjKvZCx1jW0ZD"
	var params = fb.Params{
		"fields":       "name,id",
		"access_token": accessToken,
	}

	res, err := fb.Api("/me", "GET", params)
	if err != nil {
		fmt.Errorf("Error: %q\n", err)
	}
	fmt.Printf("%v\n", res)

	params = fb.Params{
		"fields":       "friends",
		"access_token": accessToken,
	}
	res, err = fb.Api("/me", "GET", params)
	if err != nil {
		fmt.Errorf("Error: %q\n", err)
	}
	fmt.Printf("%v\n", res)

}
