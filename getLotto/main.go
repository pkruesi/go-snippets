package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type lottoNumbers struct {
	numbers []int
	lucky   []int
}

func getNumbers(amountOfNumbers int, maxValue int) []int {
	rand.Seed(time.Now().UnixNano())

	lNumbers := make([]int, amountOfNumbers)
	//initialize the slice so that we can also have zero as random number
	for i := range lNumbers {
		lNumbers[i] = -1
	}

	var n int
	var exists bool
	for i := 0; i < len(lNumbers); i++ {

		for {
			n = rand.Intn(maxValue) + 1
			exists = false
			for k := 0; k < len(lNumbers); k++ {
				if lNumbers[k] == n {
					exists = true
					break
				}
			}
			if exists != true {
				lNumbers[i] = n
				break
			}

		}
	}

	sort.Ints(lNumbers)
	return lNumbers
}

func main() {
	n := getNumbers(6, 23)
	l := getNumbers(1, 6)
	lottoNumbers := lottoNumbers{numbers: n, lucky: l}
	fmt.Printf("Lotto Zahlen: %v\n", n)
	fmt.Printf("Glücks Zahl:  %v\n", l)
	fmt.Printf("as struct: \n%v\n", lottoNumbers)
}
