package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func getUnicNumbersFromTo(amountOfNumbers int, min int, max int) []int {
	rand.Seed(time.Now().UnixNano())

	lNumbers := make([]int, amountOfNumbers)
	//initialize the slice so that we can also have zero as random number
	for i := range lNumbers {
		lNumbers[i] = -1
	}

	var n int
	var exists bool
	for i := 0; i < len(lNumbers); i++ {

		for {
			n = rand.Intn(max-min+1) + min
			exists = false
			for k := 0; k < len(lNumbers); k++ {
				if lNumbers[k] == n {
					exists = true
					break
				}
			}
			if exists != true {
				lNumbers[i] = n
				break
			}

			if exists != true {
				lNumbers[i] = n
				break
			}

		}
	}

	sort.Ints(lNumbers)
	return lNumbers
}

func getUnicNumbers(amountOfNumbers int, max int) []int {
	rand.Seed(time.Now().UnixNano())

	lNumbers := make([]int, amountOfNumbers)
	//initialize the slice so that we can also have zero as random number
	for i := range lNumbers {
		lNumbers[i] = -1
	}

	var n int
	var exists bool
	for i := 0; i < len(lNumbers); i++ {

		for {
			n = rand.Intn(max) + 1
			exists = false
			for k := 0; k < len(lNumbers); k++ {
				if lNumbers[k] == n {
					exists = true
					break
				}
			}

			if exists == false {
				lNumbers[i] = n
				break
			}

		}
	}

	sort.Ints(lNumbers)
	return lNumbers
}

func main() {
	for i := 0; i < 10; i++ {
		fmt.Printf("%v ", getUnicNumbersFromTo(3, 0, 4))
		fmt.Printf("%v\n\n ", getUnicNumbers(3, 4))
	}
}
