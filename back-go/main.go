package main

import (
	"flag"
	"fmt"

	"conf"
)

var configFile string

func init() {
	// parse arguments
	flag.StringVar(&configFile, "configfile", "config.json", "Configurationfile to read.")
	flag.Parse()
}

func main() {
	fmt.Printf("Flag: %s\n", configFile)
	conf.ReadConfig(&configFile)
	fmt.Println(configFile)

}
