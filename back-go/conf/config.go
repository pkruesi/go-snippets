package conf

import (
	"fmt"
	"os"
)

func ReadConfig(f *string) {

	if _, err := os.Stat("/" + *f); err != nil {
		fmt.Errorf("File not exists: %v\n", err)

	}

	fmt.Printf("configfile: %s\n", *f)
}
