package main

// wird mit go run *go aufgerufen
// nur so wird auf die anderen *go referenziert!

import (
	"fmt"
)

func main() {
	fmt.Println("hello world")
	debugprinter("this is a test")
}
