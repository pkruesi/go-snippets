package main

import (
	"fmt"
	"os"
)

func main() {
	args := os.Args[0:]

	fmt.Printf("len(os.Args[]): %d\n", len(args))
	fmt.Printf("os.Args[1]: %s\n", args)
}
