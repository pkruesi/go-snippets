package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"gopkg.in/yaml.v3"
)

type Blacklist struct {
  Emails []string `yaml:"Emails"`
}

func main(){

  var blacklist Blacklist
  data, err := os.ReadFile("blacklist.yml")
  if err != nil {
    log.Fatalln(err)
  }

  yaml.Unmarshal(data, &blacklist)

  max := 6

  for i, email := range blacklist.Emails {
    if (i % max == 0) && (i != 0)  {
      fmt.Printf("Sleep\n")
      time.Sleep(5*time.Second) 
    }
    fmt.Printf("index: %d email: %s\n", i, email)
  }

}
