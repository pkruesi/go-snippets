package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Println("check exists")
	path := "/blumfrup"
	if _, err := os.Stat(path); os.IsNotExist(err) {
		fmt.Printf("Path \"%s\" not exists.\n", path)
	}

	path = "/tmp"
	if _, err := os.Stat("/tmp"); err == nil {
		fmt.Printf("Path \"%s\" exists.\n", path)
	}
}
