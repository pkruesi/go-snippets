package main

import (
	"bufio"
	"fmt"
	"io"
	"net/url"
	"os"
)

func main() {

	info, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	if info.Mode()&os.ModeCharDevice == os.ModeCharDevice {
		fmt.Println("no pipe!")
		os.Exit(1)
	}

	reader := bufio.NewReader(os.Stdin)

	for {
		line, _, err := reader.ReadLine()
		if err != nil && err == io.EOF {
			break
		}
		fmt.Printf("%s\n", url.QueryEscape(string(line)))
	}

}
