package constants

const (
	PORT      = ":8080"
	INDEX     = "templates/index.html"
	HOW       = "templates/how.html"
	SHOWALL   = "templates/showall.html"
	ADDNEWHOW = "templates/addnewhow.html"
	SLN       = "templates/sln.html"
	JSONFILE  = "data/data.json"
	HOWSJSON  = "data/hows.json"
	LOGFILE   = "data/hows.log"
)
