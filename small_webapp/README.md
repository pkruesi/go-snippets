# A small test app
* reads stuff from ./data/hows.json
  * ```/highoutputword```
    * outputs a random entry from ./data/hows.json
  * ```/showall``` 
    * not implemented yet, shows all entries in ./data/hows.json

# go build
```bash
go build -o howsapp main.go
```

# Docker build
```bash
docker build -t howsapp:$(date +%F) .
```

# Docker run
```bash
docker run -d -v /var/spool/howsapp/:/data -p 8080:8080 howsapp:$(date +%F)

# example
docker run -d -v /var/spool/howsapp/:/data -p 8080:8080 howsapp:2018-08-24

# with auto restart after reboot
docker run --restart=unless-stopped -d -v /var/spool/howsapp/:/data -p 8080:8080 howsapp:2018-08-24
```
