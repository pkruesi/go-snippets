module small_webapp

go 1.17

require (
	github.com/denisbrodbeck/striphtmltags v6.6.6+incompatible
	github.com/gorilla/mux v1.8.0
)
