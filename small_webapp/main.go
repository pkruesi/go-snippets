package main

import (
	"net/http"

	"github.com/gorilla/mux"

	"small_webapp/constants"
	"small_webapp/controllers"
	"small_webapp/utils"
)

// Entry point from golang application
func main() {

	logger := utils.GetMyLogger()
	logger.Printf("Start howsapp: Port [%v]\n", constants.PORT)

	// create my router
	rtr := mux.NewRouter()
	// Router from / url and redirect to the root handler
	rtr.HandleFunc("/", controllers.RootHandler)

	// Router of /showall url
	rtr.HandleFunc("/showall", controllers.ShowAllHandler)

	// Router of /highoutputword
	rtr.HandleFunc("/highoutputword", controllers.HighOutputWordHandler)

	// Router of /addnewhow
	rtr.HandleFunc("/addnewhow", controllers.AddNewHowHandler)

	// Router of /sln
	rtr.HandleFunc("/sln", controllers.SLN)

	// Define the web root path for css, js and any asset used on the HTML templates
	//rtr.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	// Start HTTP handle
	http.Handle("/", rtr)

	// Application will listen port <nnnn>, where nnnn is configured on constants package
	logger.Println(http.ListenAndServe(constants.PORT, nil))
}
