package utils

import (
	"math/rand"
	"sort"
	"time"
)

type lottoNumbers struct {
	Numbers []int
	Lucky   []int
}

// getNumbers generates a slice of uniq random numbers
func getNumbers(amountOfNumbers int, maxValue int) []int {
	rand.Seed(time.Now().UnixNano())

	lNumbers := make([]int, amountOfNumbers)

	var n int
	var exists bool
	for i := 0; i < len(lNumbers); i++ {

		for {
			n = rand.Intn(maxValue) + 1
			exists = false
			for k := 0; k < len(lNumbers); k++ {
				if lNumbers[k] == n {
					exists = true
					break
				}
			}
			if exists != true {
				lNumbers[i] = n
				break
			}

		}
	}

	sort.Ints(lNumbers)
	return lNumbers
}

// SLN returns new random swiss lotto numbers
func SLN() lottoNumbers {

	sln := lottoNumbers{
		Numbers: getNumbers(6, 26),
		Lucky:   getNumbers(1, 6),
	}

	return sln
}
