package utils

import (
	"encoding/json"
	"io/ioutil"
)

type HOWs struct {
	Entries []string `json:"entries"`
}

func GetAllHows(howsFile string) HOWs {
	logger.Printf("GetAllHows\n")

	raw, err := ioutil.ReadFile(howsFile)
	if err != nil {
		logger.Panicf("Error ioutil.ReadFile: %s\n", err.Error())
	}

	var hows HOWs
	err = json.Unmarshal(raw, &hows)
	if err != nil {
		logger.Panicf("Error json.Unmarshal: %s\n", err.Error())
	}

	return hows
}

func AddNewHow(howsFile string, newhow string) {
	hows := GetAllHows(howsFile)
	hows.Entries = append(hows.Entries, newhow)
	logger.Printf("AddNewHow: %s\n", newhow)

	howsjson, err := json.Marshal(&hows)
	if err != nil {
		logger.Panicf("json.Marshal: %s\n", err.Error())
	}

	err = ioutil.WriteFile(howsFile, howsjson, 0644)
	if err != nil {
		logger.Panicf("ioutil.WriteFile: %s\n", err.Error())
	}
}
