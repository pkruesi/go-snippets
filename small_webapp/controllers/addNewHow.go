package controllers

import (
	"html/template"
	"net/http"
	"small_webapp/constants"
	"small_webapp/utils"
	"strings"

	"github.com/denisbrodbeck/striphtmltags"
)

// AddNewHowHandler, shows all entries
func AddNewHowHandler(w http.ResponseWriter, r *http.Request) {

	logger.Printf("%s %s\n", r.RequestURI, r.RemoteAddr)

	newhow := striphtmltags.StripTags(r.FormValue("newhow"))
	newhow = strings.TrimSpace(newhow)

	if len(newhow) > 0 {
		utils.AddNewHow(constants.HOWSJSON, newhow)
		http.Redirect(w, r, "/highoutputword", 302)
		logger.Printf("%s %s newhow: '%s'\n", r.RequestURI, r.RemoteAddr, newhow)
	}
	var hows utils.HOWs

	t, _ := template.ParseFiles(constants.ADDNEWHOW)
	t.Execute(w, hows.Entries)
}
