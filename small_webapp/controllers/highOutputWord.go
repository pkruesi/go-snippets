package controllers

import (
	"html/template"
	"math/rand"
	"net/http"
	"small_webapp/constants"
	"small_webapp/utils"
	"time"
)

// HighOutputWord, gives me a random highoutputword
func HighOutputWordHandler(w http.ResponseWriter, r *http.Request) {

	logger.Printf("%s %s\n", r.RequestURI, r.RemoteAddr)
	hows := utils.GetAllHows(constants.HOWSJSON)
	a := len(hows.Entries)
	rand.Seed(time.Now().UnixNano())
	how := hows.Entries[rand.Intn(a)]

	t, _ := template.ParseFiles(constants.HOW)
	t.Execute(w, how)
	logger.Printf("%s %s random how: '%s'\n", r.RequestURI, r.RemoteAddr, how)
}
