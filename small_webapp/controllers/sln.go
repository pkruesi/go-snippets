package controllers

import (
	"html/template"
	"net/http"

	"small_webapp/constants"
	"small_webapp/utils"
)

// sln, returns random swiss lotto numbers
func SLN(w http.ResponseWriter, r *http.Request) {
	logger.Printf("%s %s %s\n", r.RequestURI, r.RemoteAddr, r.UserAgent())
	sln := utils.SLN()
	logger.Printf("Lottonumbers: %v Luckynumber: %v\n", sln.Numbers, sln.Lucky)

	t, _ := template.ParseFiles(constants.SLN)
	t.Execute(w, sln)
}
