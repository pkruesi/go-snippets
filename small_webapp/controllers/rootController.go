package controllers

import (
	"net/http"
)

// Root handler jumps directly to /highoutputword
func RootHandler(w http.ResponseWriter, r *http.Request) {
	logger.Printf("%s %s\n", r.RequestURI, r.RemoteAddr)

	http.Redirect(w, r, "/highoutputword", 302)
}
