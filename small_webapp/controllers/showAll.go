package controllers

import (
	"html/template"
	"net/http"
	"small_webapp/constants"
	"small_webapp/utils"
)

// ShowAllHandler, shows all entries
func ShowAllHandler(w http.ResponseWriter, r *http.Request) {
	logger.Printf("%s %s\n", r.RequestURI, r.RemoteAddr)
	hows := utils.GetAllHows(constants.HOWSJSON)

	t, _ := template.ParseFiles(constants.SHOWALL)
	t.Execute(w, hows.Entries)
}
