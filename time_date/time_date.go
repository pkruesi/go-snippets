package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println(time.Now().Format(time.RFC822))
	fmt.Println(time.Now().Format(time.RFC3339))
	fmt.Println(time.Now().Date())
	date := time.Now()
	fmt.Println(date)

}
